--- src/lj_ccall.c
+++ src/lj_ccall.c
@@ -438,7 +438,7 @@
    if (ngpr > maxgpr) { \
      nsp += ngpr - 8; \
      ngpr = 8; \
-     if (nsp > CCALL_MAXSTACK) { \
+     if (nsp > CCALL_NUM_STACK) { \
        goto err_nyi; \
      } \
    } \
